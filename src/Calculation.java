/* Calculation Game - Calculation
 * Object Class for Calculation
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 29.4.2021
 */
public class Calculation {
	
	private enum arithmeticOperation{ 
		PLUS("+"){}, 
		MINUS("-"){},
		TIMES("*"){},
		DIVIDE("\\"){};
		
		private final String sign;
		
		arithmeticOperation(String sign){
			this.sign = sign;
		}
	}
	
	//class Vars
	private final int UPPER_BOUND_OF_VALUES = 101; //its a boundary so range is value-1
	
	//instance Vars
	private int num1;
	private int num2;
	private arithmeticOperation operation;
	private float solution;
	
	//Constructor
	Calculation() {
		this.num1 = Application.random.nextInt(UPPER_BOUND_OF_VALUES);
		this.num2 = Application.random.nextInt(UPPER_BOUND_OF_VALUES);
		this.operation = arithmeticOperation.values()[Application.random.nextInt(arithmeticOperation.values().length)];
		preventDivisonByZero();
		this.solution = calculateSolution();
	}
	
	//Methods
	private void preventDivisonByZero() {
		if(this.num2 == 0 && this.operation.equals(arithmeticOperation.DIVIDE)) {
			boolean stillZero = true;
				while(stillZero) {
				this.num2 = Application.random.nextInt(UPPER_BOUND_OF_VALUES);
				if(num2 != 0) {
					stillZero = false;
				}
			}
		}
	}

	private float calculateSolution() {

		float solution;

		switch (this.operation) {
		case PLUS:
			solution = (float) this.num1 + (float) this.num2;
			break;
		case MINUS:
			solution = (float) this.num1 - (float) this.num2;
			break;
		case TIMES:
			solution = (float) this.num1 * (float) this.num2;
			break;
		case DIVIDE:
			solution = (float) this.num1 / (float) this.num2;
			break;
		default:
			solution = Float.NaN;
		}

		return solution;
	}
	
	public String getExercisenAsFormatString() {
		String screenOutput = this.num1+" "+this.operation.sign+" "+this.num2+" = ";
		return screenOutput;
	}
	
	//Getter and Setter
	public float getSolution() {
		return solution;
	}
}
