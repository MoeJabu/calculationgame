import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/* Calculation Game - Application
 * contains Mainclass for Calculation Game
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 29.4.2021
 */
public class Application {
	
	public static final Scanner scanner = new Scanner(System.in);
	public static final Random random = new Random();
	public static final int NUMBER_OF_ROUNDS = 5;

	public static void main(String[] args) {
		
		int roundsSolved = 0;
		List<Participation> allParticipations = new ArrayList<Participation>();
				
		for(int i = 0; i < NUMBER_OF_ROUNDS;i++) {
			allParticipations.add(new Participation());
		}
		
		for(Participation actualParticipation:allParticipations) {
			actualParticipation.answerAnExercise();
			actualParticipation.addToAverageCalculation();
		}

		scanner.close();
		
		//Endscreen
		Participation.printEndScreenHeader();
		
		for(Participation actualParticipation:allParticipations) {
			actualParticipation.printEndScreenSingleExercise();
			if(actualParticipation.isSolved()) {
				roundsSolved++;
			}
		}
	
		Participation.printEndScreenFooter();
		
		if(roundsSolved == NUMBER_OF_ROUNDS) {
			System.out.println("Congratulations - you answered all questions correctly!");
		}
	}
}
