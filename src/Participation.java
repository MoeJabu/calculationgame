import java.text.DecimalFormat;

/* Calculation Game - Participation
 * Object Class for Participation
 * provides functionality for all
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 29.4.2021
 */
public class Participation {
	
	//class vars
	private static int overallNumberOfExercise = 0;
	private static double allTimesSummed = 0;
	
	//instance vars
	private int numberOfExercise;
	private Calculation exercise;
	private float enteredSolution;
	private boolean solved;
	private double startTimeInMs;
	private double endTimeInMs;
	private double timeTaken;
	
	//Constructor
	Participation(){
		overallNumberOfExercise++;
		this.numberOfExercise = overallNumberOfExercise;
		this.exercise = new Calculation();
		this.solved = false;
	}
	
	//privat methods
	private double calculateTakenTimeInSec() {
		double timeTaken = (this.endTimeInMs - this.startTimeInMs)*0.001;		
		return timeTaken;
	}
	
	private boolean checkIfSolutionIsCorrect() {
		boolean isCorrect = false;
		DecimalFormat twoDec = new DecimalFormat("#.##");
		
		float roundedEnteredSolution = Float.valueOf(twoDec.format(this.enteredSolution));
		float roundedExerciseSolution = Float.valueOf(twoDec.format(this.exercise.getSolution()));
		
		if(roundedEnteredSolution == roundedExerciseSolution) {
			isCorrect = true;
		}
		
		return isCorrect;
	}
	
	private static double getAverageTime() {
		double average = allTimesSummed/overallNumberOfExercise;		
		return average;
	}
	
	//public object methods
	public void answerAnExercise() {
		this.startTimeInMs = System.currentTimeMillis();
		this.enteredSolution = readFloatForPromt("Please enter the result of: "+this.exercise.getExercisenAsFormatString());
		this.endTimeInMs = System.currentTimeMillis();
		this.solved = checkIfSolutionIsCorrect();
		this.timeTaken = calculateTakenTimeInSec();	
	}
	
	public void addToAverageCalculation() {
		allTimesSummed = allTimesSummed+this.calculateTakenTimeInSec();
	}
	
	//public static methods
	public static void printEndScreenHeader() {
		System.out.println();
		System.out.println("Below you find your results");
		System.out.println();	
	}
	
	public void printEndScreenSingleExercise() {
		String indent = "  ";
		
		System.out.println("Calculation "+this.numberOfExercise+":");
		System.out.printf("%s%s%.2f\n",indent, this.exercise.getExercisenAsFormatString(),this.enteredSolution);
		System.out.println(indent+"Correct: "+this.solved);
		System.out.printf("%sTime needed: %.2f\n",indent,this.timeTaken);
	}
	
	public static void printEndScreenFooter() {
		System.out.println();
		System.out.printf("Average time needed: %.2f sec\n",getAverageTime());
	}
	
	public static float readFloatForPromt(String promt) {
		boolean isFloat = false;
		float input = Float.NaN;
		
		do {
			System.out.printf(promt);
			
			if(Application.scanner.hasNextFloat()) {
				input = Application.scanner.nextFloat();
				isFloat = true;
				if(!Application.scanner.nextLine().equals("")) {
					isFloat = false;
				}
			}else {
				Application.scanner.nextLine();
			}
			if(!isFloat) {
				System.out.println("Please enter a valid result!");
			}			
		}while(!isFloat);
		
		return input;
	}
	
	//getter and setter
	public boolean isSolved() {
		return solved;
	}
}

